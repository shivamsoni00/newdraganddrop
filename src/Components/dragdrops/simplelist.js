import React, { Component } from 'react'
import DragAndDrop from './drag'
import axios from 'axios'


class FileList extends Component {

state = {
    files: [
    "nice.pdf",
    "nice.jpg",
    "nice.png",
    ]
  }

  handleDrop = (files) => {
    let fileList = this.state.files
    console.log(fileList)
    for (var i = 0; i < files.length; i++) {
      if (!files[i].name) return
      fileList.push(files[i].name)
    }
    this.setState({files: fileList})
    console.log(files)
  }

handleAxiospost = () => {
    
    let filearray = this.state.files
    const data= JSON.stringify({file_array:filearray})
    const config = {
      headers:{'Content-Type':'application/json'}
    }
    
    return axios.post('http://localhost:3002/files',data,config)
  
}
  
render() {
    return (
      <div>
 <DragAndDrop handleDrop={this.handleDrop}>
        <div style={{height: 150,width: 250,border:'solid'}}>
          {this.state.files.map((file) =>
            <div key={file}>{file}</div>
          )}
        </div>
      </DragAndDrop>

      <div>
        <button onClick={this.handleAxiospost}>upload</button>
      </div>
</div>

     
    )
  }
}
export default FileList